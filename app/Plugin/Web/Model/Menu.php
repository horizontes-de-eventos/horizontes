<?php

class Menu extends WebAppModel {

	public $useTable = 'menus';
	public $primaryKey = 'men_id';
	public $useDbConfig = 'default';
	public $rescursive = -1;
	
	public $hasMany = array(
		'Submenu1' => array(
			'className' => 'Web.Submenu1',
			'foreignKey' => 'men_parent_id',
			'order' => array('men_posicao'=>'asc')
		)
	);

	
}