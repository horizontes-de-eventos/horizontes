<?php

class Artigo extends WebAppModel {

	public $useTable = 'secao';
	public $primaryKey = 'sec_id';
	public $useDbConfig = 'portalReader';
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Noticia']['not_data']) ) {
				$results[$key]['Noticia']['not_data'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Noticia']['not_data'] ) );
			}
		}
		}
		return $results;
	}

}