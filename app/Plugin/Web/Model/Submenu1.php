<?php

class Submenu1 extends WebAppModel {

	public $useTable = 'menus';
	public $primaryKey = 'men_id';
	public $useDbConfig = 'default';
	
	public $hasMany = array(
		'Submenu2' => array(
			'className' => 'Web.Submenu2',
			'foreignKey' => 'men_parent_id'
		)
	);

	
}