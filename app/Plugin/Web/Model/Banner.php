<?php

class Banner extends WebAppModel {

	public $useTable = 'banner';
	public $primaryKey = 'ban_id';
	public $useDbConfig = 'portalReader';
	
	public $hasMany = array(
		'BannerImg' => array(
			'className' => 'Web.BannerImg',
			'foreignKey' => 'bai_ban_id'
		)
	);
}