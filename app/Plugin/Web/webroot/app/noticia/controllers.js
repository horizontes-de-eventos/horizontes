fenapaesApp.cC({
	name: 'noticiaCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this.$.paginator = {
			page: 1
		};
		this.$.host = location.host;
		this._load();
	},
	watch: {
		'{object}paginator.page':'_load'
	},
	methods: {
		_load: function() {
			
			this.Noticia.get(
				{
					q: 'Noticia.not_ent_id.eq.1',
					sort: '-not_data',
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.noticias = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
		}
	}
});

fenapaesApp.cC({
	name: 'noticiaViewCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Noticia = this.$resource('/api/noticia/:id.json');
		this.$.host = location.host;
		this._view();
	},
	
	methods: {
		_view: function() {
			this.Noticia.get(
				{
					id: this.$routeParams.id
				}
			).$promise.
			then(function(data){
				data.data.Noticia.not_conteudo = this._handleLinks(data.data.Noticia.not_conteudo);
				this.$.noticia = data.data;
			}.bind(this));
		}
	}
});
