fenapaesApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controller: 'homeCtrl', 
		templateUrl: '/web/app/home/home.html'
	})
	.when('/apaesnosestados', {
		controller: 'mapsController',
		templateUrl: '/web/app/static/apaesnosestados.html'
	})
	.when('/comoajudar', {
		templateUrl: '/web/app/static/comoajudar.html'
	})
		.when('/faleconosco', {
		templateUrl: '/web/app/static/faleconosco.html'
	})
	.when('/oquefazemos', {
		templateUrl: '/web/app/static/oquefazemos.html'
	})
	.when('/noticia', {
		controller: 'noticiaCtrl', 
		templateUrl: '/web/app/noticia/index.html'
	})
	.when('/noticia/:id', {
		controller: 'noticiaViewCtrl', 
		templateUrl: '/web/app/noticia/view.html'
	})
	.when('/artigo/:id', {
		controller: 'artigoViewCtrl', 
		templateUrl: '/web/app/artigo/view.html'
	})
	.when('/arquivo/list/:id', {
		controller: 'arquivoListCtrl', 
		templateUrl: '/web/app/arquivo/list.html'
	})
	.when('/arquivo/:id', {
		controller: 'arquivoDownloadCtrl',
		templateUrl: '/web/app/arquivo/download.html'
	})
	.when('/foto.phtml/:id/:size', {
		controller: 'fotoCtrl', 
	})
	
	.otherwise({
		templateUrl: '/web/app/errors/404.html'
	});
	
});