fenapaesApp.service('modelService', ['$resource','DataService', function($resource, DataService) {
	this.model = {
		Entidade: {
			res: $resource('/api/entidade/:id.json'),
			params: {
				q: 'Entidade.ent_situacao.eq.i',
				sort: 'ent_fed_uf, ent_cidade'
			},
			page: 1,
			get: function(params) {
				return this.model.Entidade.res.get((params)?params:this.model.Entidade.params).$promise;
			}.bind(this),
		},
		Estado: {
			res: $resource('/api/estado/:id.json'),
			params: {
				sort: 'sigla',
				limit: 100
			},
			get: function(params) {
				return this.model.Estado.res.get((params)?params:this.model.Estado.params).$promise;
			}.bind(this),
			save: function(id, data) {
				this.model.Estado.res.save({id:id},data);
			}.bind(this)
		}
	};

}]);