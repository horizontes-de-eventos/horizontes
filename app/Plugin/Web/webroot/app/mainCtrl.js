fenapaesApp.cC({
	name: 'mainController', 
	inject: ['$scope','$resource','DataService','$rootScope'],
	init: function() {
		
		this.Entidade = this.$resource('/api/entidade/host/:host.json');
		
		this.$.version = '3.3.2';
		this.$.theme = 'lumen';
		this.$.message = 'Bem-vindo';

		this._loadEntidade();
		
	},
	methods: {
		go: function(where) {
			location.href = where;
		},
		_loadEntidade: function() {
			host = location.host;
			if (host = 'websites.apaebrasil.org.br') host = 'apaebrasil.org.br';
			this.Entidade.get({
				host: host
			}).$promise.then(function(data){
				this.DataService.Entidade = data.data.Entidade;
				this.$rootScope.$broadcast('EntidadeLoaded', true);
			}.bind(this));
		}
	}
});
