<!DOCTYPE html>

<html ng-app="fenapaesApp" ng-controller="mainController">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-Frame-Options" content="ALLOW-FROM: https://www.youtube.com/embed/">
		<title>Fenapaes</title>
		<!-- jQuery -->
		<script src="/web/bower_components/jquery/dist/jquery.js"></script>
		<style>
			/*
			.table td {
				vertical-align: middle !important;
				font-size: 15px !important;
			}
			*/
			.affix {
				z-index: 1000;
				top: 0px;
				left: 15px;
				right: 15px;
			}
			.google-map {
				height: 450px;
			}
		</style>
		<!-- Tweeter Bootstrap -->
		<link rel="stylesheet" href="/web/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" id="BootswatchTheme" href="/web/bower_components/bootswatch/lumen/bootstrap.min.css">
		<link rel="stylesheet" href="/web/bower_components/smartmenus/dist/css/sm-core-css.css">
		<link rel="stylesheet" href="/web/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.css">

		<style id="BootswatchThemeStyle">
		</style>
		<script src="/web/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<!-- Angular Js -->
		<script src="/web/bower_components/angular/angular.min.js"></script>
		<script src="/web/bower_components/angular-i18n/angular-locale_pt-br.js"></script>
		
		<script src="/web/bower_components/angular-route/angular-route.min.js"></script>
		<script src="/web/bower_components/angular-resource/angular-resource.min.js"></script>
		<script src="/web/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
		<script src="/web/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
		<script src="/web/bower_components/smartmenus/dist/jquery.smartmenus.min.js"></script>
		<script src="/web/bower_components/smartmenus/dist/addons/bootstrap/jquery.smartmenus.bootstrap.min.js"></script>
		<script src='//maps.googleapis.com/maps/api/js'></script>
		<script src="/web/bower_components/ng-maps/dist/ng-maps.min.js"></script>
		
		<!-- Locais -->
		<script src="/web/components/classy/angular-classy.min.js"></script>
		<script src="/web/bower_components/classy-on/classy-on.js"></script>
		<script src="/web/components/dialog/dialogs.min.js"></script>
		<script src="/web/components/dialog/dialogs-default-translations.min.js"></script>
		<link href="/web/components/dialog/dialogs.min.css" rel="stylesheet">

		<script src="/web/app.js"></script>
		<script src="/web/Data.js"></script>
		<script src="/web/mainCtrl.js"></script>
		<script src="/web/app/menu/controllers.js"></script>
		<script src="/web/modelService.js"></script>
		<script src="/web/routes.js"></script>
		
		<script src="/web/components/dialog/config.js"></script>

		<!-- Menus -->
		<script src="/web/app/menu/controllers.js"></script>
		<!-- Maps -->
		<script src="/web/app/maps/index.js"></script>
		
		<!-- All Controllers -->
		<?php echo $this->Element('controllers'); ?>
		
	</head>
	<body style="margin-top:10px;">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3&appId=451419348279152";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<div id="hidePage" ng-if="!hidePage" ng-cloack ng-controller="menuCtrl">
		<?php echo $this->fetch('content'); ?>
		</div>
	</body>
</html>
