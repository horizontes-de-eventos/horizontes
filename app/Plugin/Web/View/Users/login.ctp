<div style="margin-top:100px"></div>

<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Login</h3>
		</div>
		<div class="panel-body">
			<form name="loginForm" id="loginForm" method="post">
				<div class="form-group">
					<label class="control-label">Email</label>
					<input class="form-control" type="email" name="data[Atendente][email]">
				</div>
				<div class="form-group">
					<label class="control-label">Senha</label>
					<input class="form-control" type="password" name="data[Atendente][senha]">
				</div>
			</form>
		</div>
		<div class="panel-footer clearfix">
			<div class="btn-group pull-right">
				<button type="submit" form="loginForm" class="btn btn-primary">Login</button>
			</div>
		</div>
	</div>
</div>
