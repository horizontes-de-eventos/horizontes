<?php
	class tbsHelper extends AppHelper {
		
		var $helpers = array('Html');
		
		public function linkbtn($options = array()) {
			$defaults = array(
				'text' => 'Link'
			);
			$options = array_merge($defaults, $options);
			return '<a href="btn btn-default">'.$options['text'].'</a>';
		}
		
		public function jqJS() {
			echo $this->Html->script('/admin/bower_components/jquery/dist/jquery.min.js');
		}
		
		public function tbsCSS() {
			echo $this->Html->css('/admin/bower_components/bootstrap/dist/css/bootstrap.min.css');
		}
		public function tbwCSS() {
			echo $this->Html->css('/admin/bower_components/bootswatch/lumen/bootstrap.min.css', array('id'=>'BWTheme'));
		}
		
		public function tbsJS() {
			echo $this->Html->script('/admin/bower_components/bootstrap/dist/js/bootstrap.min.js');
		}
		
	}