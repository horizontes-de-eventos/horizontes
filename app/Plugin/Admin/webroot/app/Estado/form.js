sitesApp.cC({
	name: 'estadoFormCtrl',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Estado =this.ModelService.model.Estado;
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
		this.$.marker = {
			options: function(){
				return {
					draggable: true
				}
			},
			events: {
				click: function(e) {
					alert(e.latLng)
				}
			}
		};
	},
	watch: {
	},
	methods: {
		_add: function(){
			this.$.header = 'Novo Estado';
		},
		_edit: function() {
			this.$.header = 'Editar Estado';
			this.$.loading = true;
			this.Estado.get(
			{
				id: this.$routeParams.id
			}).then(function(data){
				this.$.Form = data.data.Estado;
				this.$.loading = false;
			}.bind(this));
		},
		save: function() {
			this.Estado.save(this.$.Form.id, this.$.Form);
			this.$location.path('/estados');
		},
		cancel: function() {
			this.$location.path('/estados');
		}
	}
});