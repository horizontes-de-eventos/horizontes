sitesApp.cC({
	name: 'arquivoCtrl',
	inject: ['$scope','$resource'],
	init: function() {
		this.Arquivo = this.$resource('/api/arquivo/:id.json');
		this._load();
	},
	methods: {
		_load: function() {
			this.Arquivo.get(
				{
					q: 'Arquivo.arq_ent_id.eq.1',
					sort: '-arq_data',
					populate: 'ArquivoTop'
				}
			).$promise
			.then(function(data){
				this.$.Arquivos = data.data;
			}.bind(this));
		}
	}
});

sitesApp.cC({
	name: 'arquivoFormCtrl',
	inject: ['$scope','$resource'],
	init: function() {
		this.Arquivo = this.$resource('/api/arquivo/:id.json');
	},
	methods: {
		
	}
});