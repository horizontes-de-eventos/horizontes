sitesApp.cC({
	name: 'MainController',
	inject: ['$scope','ModelService','DataService'],
	init: function() {
		this.$.selectedTheme = this.DataService.Layout.theme;
		this._bootswatch();
	},
	watch: {
		'{object}selectedTheme':'changeTheme',
	},
	methods: {
		changeTheme: function(newv) {
			if (newv != this.DataService.Layout.theme) {
				$('#BWTheme').after('<style class="BWTheme">');
				$('.BWTheme').load(this.DataService.Layout.themePath+newv+'/bootstrap.min.css', function(){
					$('#BWTheme').remove();
					this.DataService.Layout.theme = newv;
				}.bind(this));
			}
		},
		_bootswatch: function() {
			
			this.ModelService.model.Bootswatch.get().then(function(data){
				this.$.BootswatchThemes = data.themes;
			}.bind(this));
		}
	}
});