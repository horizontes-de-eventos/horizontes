sitesApp.cC({
	name: 'menuCtrl',
	inject: ['$scope','$resource','ModelService','DataService'],
	init: function() {
		this.Menu = this.$resource('/api/menu/:id.json');
		this.Menu2 = this.ModelService.model.Menu;
		this.$.loading = [
			false,
			false,
			false,
			false
		];
		this.$.norecords = [
			false,
			false,
			false,
			false
		];
		this.$.ids = [
			0,
			null,
			null,
			null
		];
		
		this.$.dragControlListeners = {
			accept: function (sourceItemHandleScope, destSortableScope) {
				return true;
			},
			itemMoved: function (event) {
				console.log('move');
				console.log(event);
			},
			orderChanged: function(event) {
				console.log('order');
				console.log(event);
			},
			containment: 'parent',
			containerPositioning: 'relative'
		};

		this.$.menuText = 'Superior';
		this.$.textoNivel = [];
		this.$.men_tipo = 'h';
		this.q = {};
		this.tipo = 'h';
		this.sort = 'men_posicao';
		this.$.Menus = [];
		this.$.level = 1;
		this.$.orderNum = 1;
		this.$.orderMax = 0;
		//this.changeToSup();
	},
	watch: {
		'{objecto}orderNum':'_doneOrderMenu()'
	},
	methods: {
		_doneOrderMenu: function(newValue,oldValue) {
			console.log(newValue);
			if (this.$.orderNum == this.$.orderMax) {
				console.log('ordering done');
			}
		},
		_delMenus: function(level) {
			for (l=level;l<=3;l++) {
				delete(this.$.Menus[l]);
				this.$.textoNivel[l] = '';
			}
		},
		deleteMenu: function(item, level) {
			if (confirm('Tem Certeza?')) {
				this.Menu.delete({id:item.Menu.men_id});
				this._loadMenus(level);
			}
		},
		saveOrderMenu: function(level) {
			menus = this.$.Menus[level];
			this.$.orderNum = 1;
			this.$.orderMax = menus.length;
			menus.forEach(function(item) {
				this.Menu2.save(item.Menu.men_id,
					{
						men_id: item.Menu.men_id,
						men_posicao: this.$.orderNum * 10,
					}
				);
				this.$.orderNum++;
			}.bind(this));
		},
		changeToSup: function() {
			this.tipo = 'h';
			this.$.men_tipo = 'h';
			this.$.menuText = 'Superior';
			this._delMenus(1);
			this.loadLevel(null, 1);
		},
		changeToInf: function() {
			this.tipo = 'v';
			this.$.men_tipo = 'v';
			this.$.menuText = 'Inferior';
			this._delMenus(1);
			this.loadLevel(null, 1);
		},

		loadLevel: function(item,level) {
			this._delMenus(level);
			if (item) this.$.ids[level] = item.Menu.men_id;
			if (level == 1) {
				this.q = 'Menu.men_ent_id.eq.1,Menu.men_tipo.eq.'+this.tipo+',Menu.men_parent_id.nu';
			} else {
				this.$.textoNivel[level] = '['+item.Menu.men_titulo+']';
				this.q = 'Menu.men_ent_id.eq.1,Menu.men_tipo.eq.'+this.tipo+',Menu.men_parent_id.eq.'+item.Menu.men_id;
			}
			this._loadMenus(level);
		},
		_loadMenus: function(level) {
			this.$.loading[level] = true;
			this.$.Menus[level] = [];
			this.Menu.get(
				{
					q: this.q,
					sort: this.sort,
					populate: 'Submenu1',
					limit: 100
				}
			).$promise
			.then(function(data){
				if (data.data.length == 0) {
					this.$.norecords[level] = true;
				} else {
					this.$.norecords[level] = false;
				}
				this.$.Menus[level] = data.data;
				this.$.loading[level] = false;
			}.bind(this));
		},
		alteraPai: function(item){
			this.$.selecionaPaiId = item.Menu.men_id;
		},
		cancelaPai: function() {
			delete(this.$.selecionaPaiId);
		},
		configPai: function(item) {
			data = {
				men_id: this.$.selecionaPaiId,
				men_parent_id: item.Menu.men_id
			};
			
			this.Menu.save(
				this.$.selecionaPaiId,
				data
			).$promise
			.then(function(data){
				this.loadLevel(false,1);
				delete(this.$.selecionaPaiId);
			}.bind(this));
			
		},
		configPaiNone: function(item) {
			data = {
				men_id: this.$.selecionaPaiId,
				men_parent_id: null
			};
			
			this.Menu.save(
				this.$.selecionaPaiId,
				data
			).$promise
			.then(function(data){
				this.loadLevel(false,1);
				delete(this.$.selecionaPaiId);
			}.bind(this));
			
		},
		addMenu: function(level) {
			this.$.Form = {
				men_ent_id: 1,
				men_fixo: false,
				men_tipo: 'h',
				men_posicao: (this.$.Menus[level].length+1)*10,
				men_parent_id: this.$.ids[level]
			}
			this.$.addEditMenu = true;
		},
		
		addLevelXCancel: function() {
			delete(this.$.addEditMenu);
		},
		
		editMenu: function(item, level) {
			delete(this.$.Form);
			this.Menu.get(
				{
					id: item.Menu.men_id,
					populate: 'Submenu1'
				}
			).$promise
			.then(function(data){
				this.$.Form = data.data.Menu;
			}.bind(this));
			this.$.addEditMenu = true;
		},
		
		saveLevelX: function(level) {
			this.Menu2.save(
				this.$.Form.men_id,
				this.$.Form
			);
			this._loadMenus(1);
			delete(this.$.addEditMenu);
		}
	}
});