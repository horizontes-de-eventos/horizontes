<?php
	class AdminAppController extends AppController {
		
		public $helpers = array(
			'Bootstrap.tbs',
			'Admin.angular',
			'Html',
			'Form'
		);
		public $components = array(
			'Auth' => array(
				'loginAction' => array(
					'controller' => 'Usuario',
					'action' => 'login',
					'plugin' => 'admin'
				),
				'authError' => 'Você não tem permissão!',
				'authenticate' => array(
					'Form' => array(
						'userModel'=>'Admin.Usuario',
						'fields' => array(
							'username' => 'usuario', //Default is 'username' in the userModel
							'password' => 'senha'  //Default is 'password' in the userModel
						)
					)
				)
			)
		);
		
		public $layout = 'Admin.theme';
		
		
		
		public function beforeFilter() {
			
		}
		
		public function index() {
			
		}
		
		public function add() {
			
		}
		
		public function edit($id = null) {
			
		}
		
		public function del($id = null) {

		}

	}