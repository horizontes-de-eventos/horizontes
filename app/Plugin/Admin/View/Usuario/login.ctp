<br/><br/>
<div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <div class="panel-title">Login</div>
      </div>
      <div class="panel-body">
        <form id="LoginForm" method="post" class="form">
          <form-group>
            <label>Usuário</label>
            <input name="Usuario[usuario]" class="form-control"/>
          </form-group>
          <form-group>
            <label>Senha</label>
            <input type="password" name="Usuario[senha]" class="form-control"/>
          </form-group>
        </form>
      </div>
      <div class="panel-footer clearfix">
        <div class="btn-group pull-right">
          <button form="LoginForm" class="btn btn-primary">Login</button>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4"></div>
</div>